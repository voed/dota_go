
// Dota Abilities Override File
"DOTAAbilities"
{
  "Version"   "1"

  "containers_lua_targeting"
  {
    // General
    //-------------------------------------------------------------------------------------------------------------
    "AbilityBehavior"       "DOTA_ABILITY_BEHAVIOR_UNIT_TARGET | DOTA_ABILITY_BEHAVIOR_POINT | DOTA_ABILITY_BEHAVIOR_HIDDEN"
    "AbilityUnitTargetTeam"     "DOTA_UNIT_TARGET_TEAM_BOTH"
    //"AbilityUnitTargetType"     "DOTA_UNIT_TARGET_ALL"
    "AbilityUnitTargetType"     "DOTA_UNIT_TARGET_ALL"
    "AbilityUnitDamageType"     "DAMAGE_TYPE_MAGICAL"
    "BaseClass"         "ability_lua"
    "AbilityTextureName"        "rubick_empty1"
    "ScriptFile"          "libraries/abilities/containers_lua_targeting"
    "MaxLevel"            "1"
    "IsCastableWhileHidden" "1"

    // Casting
    //-------------------------------------------------------------------------------------------------------------
    "AbilityCastRange"        "0"
    "AbilityCastPoint"        "0.0"

    // Time
    //-------------------------------------------------------------------------------------------------------------
    "AbilityCooldown"       "0"

    // Cost
    //-------------------------------------------------------------------------------------------------------------
    "AbilityManaCost"       "0 0 0 0"
  }

  "containers_lua_targeting_tree"
  {
    // General
    //-------------------------------------------------------------------------------------------------------------
    "AbilityBehavior"       "DOTA_ABILITY_BEHAVIOR_UNIT_TARGET | DOTA_ABILITY_BEHAVIOR_POINT | DOTA_ABILITY_BEHAVIOR_HIDDEN"
    "AbilityUnitTargetTeam"     "DOTA_UNIT_TARGET_TEAM_BOTH"
    //"AbilityUnitTargetType"     "DOTA_UNIT_TARGET_ALL"
    "AbilityUnitTargetType"     "DOTA_UNIT_TARGET_ALL | DOTA_UNIT_TARGET_TREE"
    "AbilityUnitDamageType"     "DAMAGE_TYPE_MAGICAL"
    "BaseClass"         "ability_lua"
    "AbilityTextureName"        "rubick_empty1"
    "ScriptFile"          "libraries/abilities/containers_lua_targeting_tree"
    "MaxLevel"            "1"
    "IsCastableWhileHidden" "1"

    // Casting
    //-------------------------------------------------------------------------------------------------------------
    "AbilityCastRange"        "0"
    "AbilityCastPoint"        "0.0"

    // Time
    //-------------------------------------------------------------------------------------------------------------
    "AbilityCooldown"       "0"

    // Cost
    //-------------------------------------------------------------------------------------------------------------
    "AbilityManaCost"       "0 0 0 0"
  }

  "example_ability"
  {
    "ID"              "1852"
    "AbilityBehavior"       "DOTA_ABILITY_BEHAVIOR_NO_TARGET | DOTA_ABILITY_BEHAVIOR_CHANNELLED"
    "BaseClass"           "ability_datadriven"
    "AbilityTextureName"      "holdout_blade_fury"

    // Stats
    //-------------------------------------------------------------------------------------------------------------
    "AbilityCastRange"        "1000"
    "AbilityCastPoint"        "0.0"
    "AbilityCooldown"       "10.0"
    "AbilityChannelTime"      "2.0 1.8 1.6 1.5"
    "AbilityUnitDamageType"     "DAMAGE_TYPE_PURE"
    "AbilityCastAnimation"      "ACT_DOTA_DISABLED"
    "AbilityDamage"         "400 600 800 1000"

    "precache"
    {
      "particle"          "particles/econ/generic/generic_aoe_explosion_sphere_1/generic_aoe_explosion_sphere_1.vpcf"
      "soundfile"         "soundevents/game_sounds_heroes/game_sounds_gyrocopter.vsndevts"
    }

    // Item Info
    //-------------------------------------------------------------------------------------------------------------
    "AbilityManaCost"       "300"
    "SideShop"            "1"

    "OnSpellStart"
    {
      "ApplyModifier"
      {
        "Target"    "CASTER"
        "ModifierName"  "modifier_channel_start"
      }
      "FireSound"
      {
        "EffectName"    "Hero_Gyrocopter.CallDown.Fire"
        "Target"      "CASTER"
      }
    }

    "OnChannelSucceeded"
    {
      "RemoveModifier"
      {
        "Target"        "CASTER"
        "ModifierName"      "modifier_channel_start"
      }
      "AttachEffect"
      {
        "EffectName"      "particles/econ/generic/generic_aoe_explosion_sphere_1/generic_aoe_explosion_sphere_1.vpcf"
        "EffectAttachType"    "follow_origin"
        "EffectRadius"      "%radius"
        "EffectDurationScale" "1"
        "EffectLifeDurationScale" "1"
        "EffectColorA"      "255 0 0"
        "EffectColorB"      "255 0 0"
        "Target"      "CASTER"
      }

      "Damage"
      {
        "Type"          "DAMAGE_TYPE_PURE"
        "Damage"        "%damage"
        "Target"
        {
          "Center"      "CASTER"
          "Radius"      "%radius"
          "Teams"       "DOTA_UNIT_TARGET_TEAM_ENEMY"
          "Types"       "DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
        }
      }

      "Knockback"
      {
        "Center"  "CASTER"
        "Target"
        {
          "Center"  "CASTER"
          "Radius"  "%radius"
          "Teams"   "DOTA_UNIT_TARGET_TEAM_ENEMY"
        }
        "Duration"  "%duration"
        "Distance"  "%distance"
        "Height"  "%height"
      }

      "FireSound"
      {
        "EffectName"    "Hero_Gyrocopter.CallDown.Damage"
        "Target"      "CASTER"
      }
    }

    "OnChannelFinish"
    {
      "RemoveModifier"
      {
        "Target"        "CASTER"
        "ModifierName"      "modifier_channel_start"
      }
    }

    "OnChannelInterrupted"
    {
      "RemoveModifier"
      {
        "Target"    "CASTER"
        "ModifierName"  "modifier_channel_start"
      }
    }

    "Modifiers"
    {
      "modifier_channel_start"
      {
        "OnCreated"
        {
          "AttachEffect"
          {
            "IsHidden" "1"
            "EffectName"    "particles/test_particle/channel_field_2.vpcf"//"gyro_calldown_marker_c"//"gyrocopter_call_down"
            "EffectAttachType"  "follow_origin"
            "Target"      "CASTER"

            "EffectRadius"      "%radius"
            "EffectColorA"      "255 0 0"
            "EffectColorB"      "255 0 0"

            "ControlPoints"
            {
              "00"    "50 100 5"
            }
          }
        }
      }
    }

    // Special
    //-------------------------------------------------------------------------------------------------------------
    "AbilitySpecial"
    {
      "01"
      {
        "var_type"        "FIELD_FLOAT"
        "duration"        "0.5"
      }

      "02"
      {
        "var_type"        "FIELD_INTEGER"
        "damage"        "400 600 800 1000"
      }

      "03"
      {
        "var_type"        "FIELD_INTEGER"
        "radius"        "550 550 600 650"
      }

      "04"
      {
        "var_type"        "FIELD_INTEGER"
        "distance"        "400 500 600 700"
      }

      "05"
      {
        "var_type"        "FIELD_INTEGER"
        "height"        "100 200 300 400"
      }
    }
  }

//-------------------
// Dummy Sleep

  "dummy_sleep_ability"
  {
    "BaseClass"         "ability_lua"
    "AbilityUnitTargetFlags" "DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES"

    "ScriptFile"        "dummy_sleep_ability"
  }

//-------------------

//-------------------
// Dummy stuff
//-------------------

  "dummy_unit"
  {
    "AbilityBehavior"     "DOTA_ABILITY_BEHAVIOR_PASSIVE"
    "BaseClass"           "ability_datadriven"
    //"AbilityTextureName"            "rubick_empty1"
    //"MaxLevel"                      "1"

    "Modifiers"
    {
      "dummy_unit"
      {
        "Passive"                        "1"
        //"IsHidden"                        "1"
        "States"
        {
          "MODIFIER_STATE_NO_UNIT_COLLISION"	"MODIFIER_STATE_VALUE_ENABLED"
  				"MODIFIER_STATE_NO_TEAM_MOVE_TO"	"MODIFIER_STATE_VALUE_ENABLED"
  				"MODIFIER_STATE_NO_TEAM_SELECT"	"MODIFIER_STATE_VALUE_ENABLED"
  				"MODIFIER_STATE_COMMAND_RESTRICTED"	"MODIFIER_STATE_VALUE_ENABLED"
  				"MODIFIER_STATE_ATTACK_IMMUNE"	"MODIFIER_STATE_VALUE_ENABLED"
  				"MODIFIER_STATE_INVULNERABLE"	"MODIFIER_STATE_VALUE_ENABLED"
  				//"MODIFIER_STATE_FLYING"	"MODIFIER_STATE_VALUE_ENABLED"
  				"MODIFIER_STATE_NOT_ON_MINIMAP"	"MODIFIER_STATE_VALUE_ENABLED"
  				"MODIFIER_STATE_UNSELECTABLE"	"MODIFIER_STATE_VALUE_ENABLED"
  				"MODIFIER_STATE_OUT_OF_GAME"	"MODIFIER_STATE_VALUE_ENABLED"
  				"MODIFIER_STATE_NO_HEALTH_BAR"		   "MODIFIER_STATE_VALUE_ENABLED"
        }
      }
    }
  }

  "barebones_empty1"
  {
    // General
    //-------------------------------------------------------------------------------------------------------------
    //"ID"              "5343"                            // unique ID number for this ability.  Do not change this once established or it will invalidate collected stats.
    "AbilityBehavior"       "DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_NOT_LEARNABLE"
    "MaxLevel"            "0"
    "BaseClass"           "ability_datadriven"
    "AbilityTextureName"            "rubick_empty1"
  }

  "barebones_empty2"
  {
    // General
    //-------------------------------------------------------------------------------------------------------------
    //"ID"              "5343"                            // unique ID number for this ability.  Do not change this once established or it will invalidate collected stats.
    "AbilityBehavior"       "DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_NOT_LEARNABLE"
    "MaxLevel"            "0"
    "BaseClass"           "ability_datadriven"
    "AbilityTextureName"            "rubick_empty1"
  }

  "barebones_empty3"
  {
    // General
    //-------------------------------------------------------------------------------------------------------------
    //"ID"              "5343"                            // unique ID number for this ability.  Do not change this once established or it will invalidate collected stats.
    "AbilityBehavior"       "DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_NOT_LEARNABLE"
    "MaxLevel"            "0"
    "BaseClass"           "ability_datadriven"
    "AbilityTextureName"            "rubick_empty1"
  }

  "barebones_empty4"
  {
    // General
    //-------------------------------------------------------------------------------------------------------------
    //"ID"              "5343"                            // unique ID number for this ability.  Do not change this once established or it will invalidate collected stats.
    "AbilityBehavior"       "DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_NOT_LEARNABLE"
    "MaxLevel"            "0"
    "BaseClass"           "ability_datadriven"
    "AbilityTextureName"            "rubick_empty1"
  }

  "barebones_empty5"
  {
    // General
    //-------------------------------------------------------------------------------------------------------------
    //"ID"              "5343"                            // unique ID number for this ability.  Do not change this once established or it will invalidate collected stats.
    "AbilityBehavior"       "DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_NOT_LEARNABLE"
    "MaxLevel"            "0"
    "BaseClass"           "ability_datadriven"
    "AbilityTextureName"            "rubick_empty1"
  }

  "barebones_empty6"
  {
    // General
    //-------------------------------------------------------------------------------------------------------------
    //"ID"              "5343"                            // unique ID number for this ability.  Do not change this once established or it will invalidate collected stats.
    "AbilityBehavior"       "DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_NOT_LEARNABLE"
    "MaxLevel"            "0"
    "BaseClass"           "ability_datadriven"
    "AbilityTextureName"            "rubick_empty1"
  }

  "explosive_tome_explode"
  {
    "BaseClass"       "ability_datadriven"
    "AbilityBehavior"   "DOTA_ABILITY_BEHAVIOR_NO_TARGET | DOTA_ABILITY_BEHAVIOR_CHANNELLED"
    "AbilityTextureName"    "techies_suicide"

    "AbilityChannelTime"      "70.0 55.0 40.0 40.0"

    "AbilityUnitTargetTeam"    "DOTA_UNIT_TARGET_TEAM_BOTH"
    "AbilityUnitDamageType"   "DAMAGE_TYPE_PURE"
    "AbilityUnitTargetType"   "DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
    "SpellImmunityType"       "SPELL_IMMUNITY_ENEMIES_YES"

    "precache"
    {
      "soundfile"       "soundevents/game_sounds_heroes/game_sounds_techies.vsndevts"
      "particle"        "particles/units/heroes/hero_techies/techies_remote_mines_detonate.vpcf"
    }

    "OnAbilityPhaseStart"
    {
      "RunScript"
      {
        "ScriptFile"    "scripts/vscripts/items/explosive_tome.lua"
        "Function"      "explosive_tome_plant"
        "Target"        "CASTER"
      }
    }

    "OnChannelSucceeded"
    {
      "RunScript"
      {
        "ScriptFile"      "scripts/vscripts/items/explosive_tome.lua"
        "Function"        "explosive_tome_explode"
        "Target"          "CASTER"
      }
    }

    "OnChannelInterrupted"
    {
      "RunScript"
      {
        "ScriptFile"      "scripts/vscripts/items/explosive_tome.lua"
        "Function"        "explosive_tome_defuse"
        "Target"          "CASTER"
      }
    }
  }

  "explosive_tome_modifier"
  {
    "BaseClass"         "ability_datadriven"
    "AbilityBehavior"     "DOTA_ABILITY_BEHAVIOR_PASSIVE"
    "AbilityTextureName"    "enchantress_untouchable"

    "Modifiers"
    {
      "modifier_explosive_tome_magic_immune"
      {
        "Passive"     "1"
        "IsHidden"    "1"

        "States"
        {
          "MODIFIER_STATE_MAGIC_IMMUNE" "MODIFIER_STATE_VALUE_ENABLED"
          "MODIFIER_STATE_INVULNERABLE" "MODIFIER_STATE_VALUE_ENABLED"
        }
      }
    }
  }

  "chronosphere_fix"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"				"ability_datadriven"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_AOE | DOTA_ABILITY_BEHAVIOR_POINT"
		"AbilityType"					"DOTA_ABILITY_TYPE_ULTIMATE"
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_YES"
		"FightRecapLevel"				"2"
		"AbilityTextureName"			"faceless_void_chronosphere"
		"AOERadius"						"%radius"

		// Precache
		//-------------------------------------------------------------------------------------------------------------
		"precache"
		{
			"soundfile"			"soundevents/game_sounds_heroes/game_sounds_faceless_void.vsndevts"
			"particle"			"particles/units/heroes/hero_faceless_void/faceless_void_chronosphere.vpcf"
			"particle"			"particles/units/heroes/hero_faceless_void/faceless_void_chrono_speed.vpcf"
		}

		// Casting
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCastRange"				"600"
		"AbilityCastPoint"				"0.35 0.35 0.35"

		// Time
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCooldown"				"130.0 115.0 100.0"

		// Cost
		//-------------------------------------------------------------------------------------------------------------
		"AbilityManaCost"				"150 225 300"

		// Stats
		//-------------------------------------------------------------------------------------------------------------
		"AbilityModifierSupportBonus"		"50"

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"				"FIELD_INTEGER"
				"radius"				"425"
			}
			"02"
			{
				"var_type"				"FIELD_FLOAT"
				"duration"				"4.0 4.5 5.0"
			}
			"03"
			{
				"var_type"				"FIELD_FLOAT"
				"duration_scepter"		"4.0 5.0 6.0"
			}
			"04"
			{
				"var_type"				"FIELD_FLOAT"
				"cooldown_scepter"		"60.0 60.0 60.0"
			}
			"05"
			{
				"var_type"				"FIELD_INTEGER"
				"vision_radius"			"475"
			}
			// Extra
			"06"
			{
				"var_type"				"FIELD_FLOAT"
				"aura_interval"			"0.1"
			}
			// If you want the Chronosphere to ignore Faceless Void then keep it at 1 otherwise set it to 0
			"07"
			{
				"var_type"				"FIELD_INTEGER"
				"ignore_void"			"1"
			}
			"08"
			{
				"var_type"				"FIELD_INTEGER"
				"speed"					"1000"
			}
		}

		"OnSpellStart"
		{
			"FireSound"
			{
				"EffectName"		"Hero_FacelessVoid.Chronosphere"
				"Target"			"CASTER"
			}

			"RunScript"
			{
				"ScriptFile"		"scripts/vscripts/heroes/hero_faceless_void/chronosphere.lua"
				"Function"			"Chronosphere"
				"Target"			"POINT"
				"dummy_aura"		"modifier_chronosphere_aura_datadriven"
			}
		}

		"Modifiers"
		{
			// Cosmetic
			"modifier_chronosphere_tooltip_datadriven"
			{
				"IsDebuff"		"1"
				"IsPurgable"	"0"
			}

			"modifier_chronosphere_aura_datadriven"
			{
				"ThinkInterval"		"%aura_interval"

				"OnIntervalThink"
				{
					"ActOnTargets"
					{
						"Target"
						{
							"Center"	"TARGET"
							"Types"		"DOTA_UNIT_TARGET_ALL"
							"Teams"		"DOTA_UNIT_TARGET_TEAM_FRIENDLY | DOTA_UNIT_TARGET_TEAM_ENEMY"
							"Flags"     "DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES"
							"Radius"	"%radius"
						}

						"Action"
						{
							"RunScript"
							{
								"ScriptFile"		"scripts/vscripts/heroes/hero_faceless_void/chronosphere.lua"
								"Function"			"ChronosphereAura"
								"Target"			"TARGET"
								"aura_modifier"		"modifier_chronosphere_datadriven"
							}
						}
					}
				}

				"States"
				{
					"MODIFIER_STATE_INVULNERABLE" 	"MODIFIER_STATE_VALUE_ENABLED"
					"MODIFIER_STATE_NO_HEALTH_BAR" 	"MODIFIER_STATE_VALUE_ENABLED"
					"MODIFIER_STATE_NOT_ON_MINIMAP" "MODIFIER_STATE_VALUE_ENABLED"
					"MODIFIER_STATE_UNSELECTABLE" 	"MODIFIER_STATE_VALUE_ENABLED"
				}
			}

			"modifier_chronosphere_datadriven"
			{
				"IsHidden"	"1"

				"OnCreated"
				{
					"ApplyModifier"
					{
						"ModifierName"	"modifier_chronosphere_tooltip_datadriven"
						"Target"		"TARGET"
					}
				}

				"OnDestroy"
				{
					"RemoveModifier"
					{
						"ModifierName"	"modifier_chronosphere_tooltip_datadriven"
						"Target"		"TARGET"
					}
				}

				"States"
				{
					"MODIFIER_STATE_STUNNED"		"MODIFIER_STATE_VALUE_ENABLED"
					"MODIFIER_STATE_FROZEN"			"MODIFIER_STATE_VALUE_ENABLED"
				}
			}
		}
	}


  "wraith_king_reincarnation_fix"
  {
    // General
    //-------------------------------------------------------------------------------------------------------------
    "BaseClass"				"ability_datadriven"
    "AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE"
		"AbilityType"					"DOTA_ABILITY_TYPE_ULTIMATE"
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_NO"
		"FightRecapLevel"				"2"
		"AbilityTextureName"			"skeleton_king_reincarnation"

    // Precache
    //-------------------------------------------------------------------------------------------------------------
    "precache"
		{
			"soundfile"	"soundevents/game_sounds_heroes/game_sounds_skeletonking.vsndevts"
			"particle"	"particles/units/heroes/hero_skeletonking/wraith_king_reincarnate.vpcf"
			"particle"	"particles/units/heroes/hero_skeletonking/skeleton_king_death.vpcf"
			"particle"	"particles/units/heroes/hero_skeletonking/wraith_king_reincarnate_slow_debuff.vpcf"
		}

    // Time
    //-------------------------------------------------------------------------------------------------------------
		"AbilityCooldown"				"260 160 60"

    // Cost
    //-------------------------------------------------------------------------------------------------------------
		"AbilityManaCost"				"160"

    // Special
    //-------------------------------------------------------------------------------------------------------------
    "AbilitySpecial"
    {
      "01"
			{
				"var_type"				"FIELD_FLOAT"
				"reincarnate_time"		"6.0 6.0 6.0"
			}
			"02"
			{
				"var_type"				"FIELD_INTEGER"
				"slow_radius"			"900"
			}
			"03"
			{
				"var_type"				"FIELD_INTEGER"
				"movespeed"				"-75"
			}
			"04"
			{
				"var_type"				"FIELD_INTEGER"
				"attackslow_tooltip"	"-75"
			}
			"05"
			{
				"var_type"				"FIELD_FLOAT"
				"slow_duration"			"5.0"
			}
      "06"
      {
        "var_type"			"FIELD_FLOAT"
        "min_health"		"1"
      }

      "07"
      {
        "var_type"      "FIELD_INTEGER"
        "regen_pct"     "17"
      }
    }

    "Modifiers"
    {
      "modifier_reincarnation_min_health"
      {
        "Passive"			"1"
        "IsHidden"			"1"
        "IsDebuff"			"0"
        "IsPurgable"		"0"
        "IsBuff"			"0"

        "Properties"
        {
          "MODIFIER_PROPERTY_MIN_HEALTH" "%min_health"
        }
      }

      "modifier_reincarnation"
			{
				"Passive"	"1"
				"IsHidden"	"1"
        "IsPurgable" "0"

				"OnTakeDamage"
				{
					"RunScript"
					{
						"ScriptFile"		"heroes/hero_skeleton_king/reincarnation.lua"
						"Function"			"Reincarnation"
					}
				}
			}

      "modifier_reincarnation_reincarnating"
      {
        "Passive"	"0"
        "IsHidden"	"0"
        "IsPurgable"  "0"
        "Duration"  "%reincarnate_time"

        "EffectName"		"particles/units/heroes/hero_skeletonking/wraith_king_reincarnate_slow_debuff.vpcf"
				"EffectAttachType"	"follow_origin"

        "States"
        {
            "MODIFIER_STATE_INVULNERABLE" "MODIFIER_STATE_VALUE_ENABLED"
            "MODIFIER_STATE_MAGIC_IMMUNE" "MODIFIER_STATE_VALUE_ENABLED"
            "MODIFIER_STATE_STUNNED" "MODIFIER_STATE_VALUE_ENABLED"
        }

        "Properties"
        {
          "MODIFIER_PROPERTY_HEALTH_REGEN_PERCENTAGE" "%regen_pct"
        }
      }

      "modifier_reincarnation_slow"
      {
        "IsDebuff"	"1"
        "Duration"	"%slow_duration"

        "EffectName"		"particles/units/heroes/hero_skeletonking/wraith_king_reincarnate_slow_debuff.vpcf"
        "EffectAttachType"	"follow_origin"

        "Properties"
        {
          "MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE"	"%movespeed"
          "MODIFIER_PROPERTY_ATTACKSPEED_BONUS_CONSTANT"	"%attackslow_tooltip"
        }
      }
    }
  }
}
