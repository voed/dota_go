dummy_sleep_ability = class({})
LinkLuaModifier( "dummy_sleep_modifier", LUA_MODIFIER_MOTION_NONE )

function dummy_sleep_ability:OnSpellStart()
  local hCaster = self:GetCaster()
  local hTarget = self:GetCursorTarget()

  if hCaster == nil or hTarget == nil then
    print("caster or target was nil")
    return
  end

  hTarget:AddNewModifier(hCaster, self, "dummy_sleep_modifier", { duration = 30 } )
end
