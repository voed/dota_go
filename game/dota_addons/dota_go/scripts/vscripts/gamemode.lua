-- This is the primary barebones gamemode script and should be used to assist in initializing your game mode


-- Set this to true if you want to see a complete debug output of all events/processes done by barebones
-- You can also change the cvar 'barebones_spew' at any time to 1 or 0 for output/no output
BAREBONES_DEBUG_SPEW = false

LOSING_STREAK_GOLD = {600, 700, 800, 1000, 1200}
WIN_ROUND_GOLD = 500

TOME_LEVEL_PER_ROUND_SHORT_TABLE = {1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3}
TOME_LEVEL_PER_ROUND_MEDIUM_TABLE = {1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3}
TOME_LEVEL_PER_ROUND_LONG_TABLE = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3}

GOLD_BOUNTY_PER_ROUND_SHORT_TABLE = {200, 200, 200, 200, 200, 300, 400, 400, 400, 400, 400}
GOLD_BOUNTY_PER_ROUND_MEDIUM_TABLE = {200, 200, 200, 200, 200, 300, 300, 300, 300, 300, 400, 400, 400, 400, 400, 500, 500, 500, 500, 600, 800 }
GOLD_BOUNTY_PER_ROUND_LONG_TABLE = {200, 200, 200, 200, 200, 200, 200, 300, 300, 300, 300, 300, 300, 300, 400, 400, 400, 400, 400, 400, 500, 500, 500, 500, 500, 500, 500, 500, 600, 700, 800}

XP_LEVEL_PER_ROUND_SHORT_TABLE = {1, 3, 6, 8, 10, 11, 13, 16, 19, 22, 25}
XP_LEVEL_PER_ROUND_MEDIUM_TABLE = {1, 2, 3, 4, 5, 6, 6, 7, 8, 9, 10, 11, 11, 12, 13, 14, 15, 16, 19, 21, 25}
XP_LEVEL_PER_ROUND_LONG_TABLE = {1, 2, 3, 4, 5, 6, 6, 7, 7, 8, 9, 10, 11, 11, 12, 12, 13, 14, 15, 16, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 25}

if GameMode == nil then
    DebugPrint( '[BAREBONES] creating barebones game mode' )
    _G.GameMode = class({})
end

-- This library allow for easily delayed/timed actions
require('libraries/timers')
-- This library can be used for advancted physics/motion/collision of units.  See PhysicsReadme.txt for more information.
require('libraries/physics')
-- This library can be used for advanced 3D projectile systems.
require('libraries/projectiles')
-- This library can be used for sending panorama notifications to the UIs of players/teams/everyone
require('libraries/notifications')
-- This library can be used for starting customized animations on units from lua
require('libraries/animations')
-- This library can be used for performing "Frankenstein" attachments on units
require('libraries/attachments')


-- These internal libraries set up barebones's events and processes.  Feel free to inspect them/change them if you need to.
require('internal/gamemode')
require('internal/events')

-- settings.lua is where you can specify many different properties for your game mode and is one of the core barebones files.
require('settings')
-- events.lua is where you can specify the actions to be taken when any event occurs and is one of the core barebones files.
require('events')

-- CUSTOM GAME EVENTS --
function GameMode:OnPlayerVoted(table)
  local player_id = table.PlayerID
  PrintTable(table)
  if not GameRules.GameMode:PlayerHasVoted(player_id) then
    print("voting!")
    local game_length_vote = table.data.category
    local data = {playerID = tonumber(player_id), category = game_length_vote}
    GameRules.GameMode.players_voted[player_id] = true

    if game_length_vote == 'Short' then
      GameRules.GameMode.short_game_votes = GameRules.GameMode.short_game_votes + 1
    elseif game_length_vote == 'Medium' then
      GameRules.GameMode.medium_game_votes = GameRules.GameMode.medium_game_votes + 1
    elseif game_length_vote == 'Long' then
      GameRules.GameMode.long_game_votes = GameRules.GameMode.long_game_votes + 1
    else
      print("??? this vote is invalid")
    end
    CustomGameEventManager:Send_ServerToAllClients("display_vote", data)
  end
end

function GameMode:PlayerHasVoted(player_id)
  return self.players_voted[player_id] ~= nil
end
-- END CUSTOM GAME EVENTS --

--[[
  This function should be used to set up Async precache calls at the beginning of the gameplay.

  In this function, place all of your PrecacheItemByNameAsync and PrecacheUnitByNameAsync.  These calls will be made
  after all players have loaded in, but before they have selected their heroes. PrecacheItemByNameAsync can also
  be used to precache dynamically-added datadriven abilities instead of items.  PrecacheUnitByNameAsync will
  precache the precache{} block statement of the unit and all precache{} block statements for every Ability#
  defined on the unit.

  This function should only be called once.  If you want to/need to precache more items/abilities/units at a later
  time, you can call the functions individually (for example if you want to precache units in a new wave of
  holdout).

  This function should generally only be used if the Precache() function in addon_game_mode.lua is not working.
]]
function GameMode:PostLoadPrecache()
  DebugPrint("[BAREBONES] Performing Post-Load precache")
  --PrecacheItemByNameAsync("item_example_item", function(...) end)
  --PrecacheItemByNameAsync("example_ability", function(...) end)

  --PrecacheUnitByNameAsync("npc_dota_hero_viper", function(...) end)
  --PrecacheUnitByNameAsync("npc_dota_hero_enigma", function(...) end)
end

--[[
  This function is called once and only once as soon as the first player (almost certain to be the server in local lobbies) loads in.
  It can be used to initialize state that isn't initializeable in InitGameMode() but needs to be done before everyone loads in.
]]
function GameMode:OnFirstPlayerLoaded()
  DebugPrint("[BAREBONES] First Player has loaded")
end

--[[
  This function is called once and only once after all players have loaded into the game, right as the hero selection time begins.
  It can be used to initialize non-hero player state or adjust the hero selection (i.e. force random etc)
]]
function GameMode:OnAllPlayersLoaded()
  DebugPrint("[BAREBONES] All Players have loaded into the game")

  self.heroes_loaded = 0

  self.short_game_votes = 0
  self.medium_game_votes = 0
  self.long_game_votes = 0
  self.game_length = 'Short'
  self.rounds_to_win = 6

  self.players_picked = {}
  self.players_voted = {}

  self._currentRound = 1
  self._badGuyRoundCount = 0
  self._goodGuyRoundCount = 0
  self._badGuyLosingStreak = 0
  self._goodGuyLosingStreak = 0

  self.tomePlanted = false
  self.tome = nil

  self.roundOver = false

  self._sleeper = CreateUnitByName("npc_dummy_unit", Vector(0,0,0), true, nil, nil, 0)
  self._sleepAbility = self._sleeper:FindAbilityByName("dummy_sleep_ability")

  self.hero_clip_checker = CreateUnitByName("npc_dummy_unit", Vector(0, 0, 0), true, nil, nil, 0)

  self._tomePlantingTeam = DOTA_TEAM_GOODGUYS
  self._tomePlantingGoodGuysIndex = 0
  self._tomePlantingBadGuysIndex = 0

  self._spawnLocations = {[DOTA_TEAM_GOODGUYS] = {}, [DOTA_TEAM_BADGUYS] = {}}

  --uncomment for customheroselector
  --CustomGameEventManager:Send_ServerToAllClients("CustomHeroSelectorShow", {})

  EmitAnnouncerSound("announcer_ann_custom_mode_15")
end

function GameMode:OnPreGameBegins()
  self:_MakeAllHeroesSleep()

  self:_ForcePickHeroes()
end

function GameMode:StartVoting()
  CustomGameEventManager:Send_ServerToAllClients("vote_panel_display", {})

  local vote_time_remaining = 15
  self.votingTimer = Timers:CreateTimer(function()
    CustomGameEventManager:Send_ServerToAllClients("update_countdown", {value = vote_time_remaining})
    vote_time_remaining = vote_time_remaining - 1
    if vote_time_remaining < 0 then
      Timers:RemoveTimer(self.votingTimer)
      self:VotingEnded()
    else
      return 1.0
    end
  end)
end

function GameMode:VotingEnded()
    CustomGameEventManager:Send_ServerToAllClients("vote_panel_hide", {})
    self:DetermineGameLength()
    CustomGameEventManager:Send_ServerToAllClients("game_length_selected", {game_length = self.game_length})

    Timers:CreateTimer(6, function()
      CustomGameEventManager:Send_ServerToAllClients("hide_game_length", {})
    end)

    Timers:CreateTimer(7, function()
      self:_GrantSpecialItems()
    end)

    Timers:CreateTimer(8, function()
      Notifications:BottomToTeam(self._tomePlantingTeam, {text="Plant the tome in designated areas.", duration = 7.0})
      self:_HintTomePlantLocations()

      if self._tomePlantingTeam == DOTA_TEAM_BADGUYS then
        Notifications:BottomToTeam(DOTA_TEAM_GOODGUYS, {text="Prevent the other team from detonating the tome.", duration = 7.0})
      elseif self._tomePlantingTeam == DOTA_TEAM_GOODGUYS then
        Notifications:BottomToTeam(DOTA_TEAM_BADGUYS, {text="Prevent the other team from detonating the tome.", duration = 7.0})
      end
    end)

    Timers:CreateTimer(15, function()
      EmitAnnouncerSound("announcer_ann_custom_timer_sec_10")
    end)

    Timers:CreateTimer(25, function()
      self:BeginGame()
    end)
end

function GameMode:DetermineGameLength()
  local max_votes = math.max(self.short_game_votes, self.medium_game_votes, self.long_game_votes)
  if max_votes == self.short_game_votes and max_votes == self.medium_game_votes and max_votes == self.long_game_votes then
    self.game_length = 'Short'
  elseif max_votes == self.short_game_votes and max_votes == self.medium_game_votes then
    self.game_length = 'Short'
  elseif max_votes == self.medium_game_votes and max_votes == self.long_game_votes then
    self.game_length = 'Medium'
  elseif max_votes == self.short_game_votes and max_votes == self.long_game_votes then
    self.game_length = 'Short'
  elseif max_votes == self.short_game_votes then
    self.game_length = 'Short'
  elseif max_votes == self.medium_game_votes then
    self.game_length = 'Medium'
  elseif max_votes == self.long_game_votes then
    self.game_length = 'Long'
  end

  if self.game_length == 'Short' then
    self.rounds_to_win = 6
    self.xp_table = XP_LEVEL_PER_ROUND_SHORT_TABLE
    self.gold_bounty_table = GOLD_BOUNTY_PER_ROUND_SHORT_TABLE
    self.tome_level_table = TOME_LEVEL_PER_ROUND_SHORT_TABLE
  elseif self.game_length == 'Medium' then
    self.rounds_to_win = 11
    self.xp_table = XP_LEVEL_PER_ROUND_MEDIUM_TABLE
    self.gold_bounty_table = GOLD_BOUNTY_PER_ROUND_MEDIUM_TABLE
    self.tome_level_table = TOME_LEVEL_PER_ROUND_MEDIUM_TABLE
  else
    self.rounds_to_win = 16
    self.xp_table = XP_LEVEL_PER_ROUND_LONG_TABLE
    self.gold_bounty_table = GOLD_BOUNTY_PER_ROUND_LONG_TABLE
    self.tome_level_table = TOME_LEVEL_PER_ROUND_LONG_TABLE
  end
end

function GameMode:_ForcePickHeroes()
  for nPlayerID = 0, DOTA_MAX_TEAM_PLAYERS-1 do
    local player = PlayerResource:GetPlayer(nPlayerID)
    if player ~= nil then
      if not PlayerResource:HasSelectedHero(nPlayerID) then
        player:MakeRandomHeroSelection()
        PlayerResource:SetHasRandomed(nPlayerID)
        PlayerResource:SetHasRepicked(nPlayerID)
      end

      --Timers:CreateTimer(7, function()
      --  self:_ForceLoadIntoGame(nPlayerID)
      --end)
    end
  end

  --Timers:CreateTimer(7.1, function()
  --  self:_SetScoreboardHeroes()
  --  self:StartVoting()
  --end)
end

function GameMode:_CheckAllHeroesInGame()
  if self:AllHeroesInGame() then
    Timers:CreateTimer(0.1, function()
      self:_SetScoreboardHeroes()
      self:StartVoting()
    end)
  end
end

function GameMode:AllHeroesInGame()
  return PlayerResource:GetTeamPlayerCount() == self.heroes_loaded
end

function GameMode:_ForceLoadIntoGame(nPlayerID)
  local player = PlayerResource:GetPlayer(nPlayerID)
  local hero_name = PlayerResource:GetSelectedHeroName(nPlayerID)
  if player:GetAssignedHero() == nil then
    CreateHeroForPlayer(hero_name, player)
  end
end

function GameMode:_SetScoreboardHeroes()
  for nPlayerID = 0, DOTA_MAX_TEAM_PLAYERS-1 do
    local player = PlayerResource:GetPlayer(nPlayerID)
    if player ~= nil then
      local hero = PlayerResource:GetSelectedHeroEntity(nPlayerID)

      local table = {
        heroname = hero:GetName(),
        team = hero:GetTeam(),
        nthIndex = self:_FindNthIndexOfPlayer(hero)
      }

      CustomGameEventManager:Send_ServerToAllClients( "hero_spawned", table )
    end
  end
end

--[[
  This function is called once and only once for every player when they spawn into the game for the first time.  It is also called
  if the player's hero is replaced with a new hero for any reason.  This function is useful for initializing heroes, such as adding
  levels, changing the starting gold, removing/adding abilities, adding physics, etc.

  The hero parameter is the hero entity that just spawned in
]]
function GameMode:OnHeroInGame(hero)
  DebugPrint("[BAREBONES] Hero spawned in game for first time -- " .. hero:GetUnitName())

  local playerID = hero:GetPlayerID()

  hero:SetGold(0, false)
  hero:SetGold(800, true)
  hero:SetMinimumGoldBounty(300)
  hero:SetMaximumGoldBounty(300)
  hero:SetDeathXP(0)

  self._sleeper:SetCursorCastTarget(hero)
  self._sleepAbility:OnSpellStart()

  Timers:CreateTimer(0.03, function()
    local nthIndex = self:_FindNthIndexOfPlayer(hero)
    hero:SetOrigin(self:_SpawnLocation(hero:GetTeam(), nthIndex))
    GameMode:_RetargetPlayerCamera(playerID)
  end)

  self.heroes_loaded = self.heroes_loaded + 1
  self:_CheckAllHeroesInGame()

  -- These lines will create an item and add it to the player, effectively ensuring they start with the item
  --local item = CreateItem("item_example_item", hero, hero)
  --hero:AddItem(item)

  --[[ --These lines if uncommented will replace the W ability of any hero that loads into the game
    --with the "example_ability" ability

  local abil = hero:GetAbilityByIndex(1)
  hero:RemoveAbility(abil:GetAbilityName())
  hero:AddAbility("example_ability")]]
end

function GameMode:BeginGame()
  self:_WakeUpHeroes()
  self:_AnnounceRoundSound(1)
  Timers:CreateTimer(1.5,
  function()
    EmitAnnouncerSound("announcer_ann_custom_begin")
  end)
  Notifications:BottomToAll({text="Round 1 begin.", duration=3.0})

  Timers:CreateTimer(3, function()
    EmitAnnouncerSound("announcer_ann_custom_timer_02")
  end)

  self:_StartPlantTimers()
  CustomGameEventManager:Send_ServerToAllClients("round_start", {})
end

--[[
  This function is called once and only once when the game completely begins (about 0:00 on the clock).  At this point,
  gold will begin to go up in ticks if configured, creeps will spawn, towers will become damageable etc.  This function
  is useful for starting any game logic timers/thinkers, beginning the first round, etc.
]]
function GameMode:OnGameInProgress()
  DebugPrint("[BAREBONES] The game has officially begun")

  -- want day night cycle to be 1 minute
  --GameRules:SetTimeOfDay(0.5)
  --Timers:CreateTimer(60,
  --function()
  --  GameRules:SetTimeOfDay(0)
  --end)
end

-- This function initializes the game mode and is called before anyone loads into the game
-- It can be used to pre-initialize any values/tables that will be needed later
function GameMode:InitGameMode()
  GameMode = self
  DebugPrint('[BAREBONES] Starting to load Barebones gamemode...')
  -- Call the internal function to set up the rules/behaviors specified in constants.lua
  -- This also sets up event hooks for all event handlers in events.lua
  -- Check out internals/gamemode to see/modify the exact code
  GameMode:_InitGameMode()

  -- Commands can be registered for debugging purposes or as functions that can be called by the custom Scaleform UI
  Convars:RegisterCommand( "command_example", Dynamic_Wrap(GameMode, 'ExampleConsoleCommand'), "A console command example", FCVAR_CHEAT )

  Convars:RegisterCommand("trigger_good_guys_win", Dynamic_Wrap(GameMode, 'TriggerGoodGuysWin'), 'Trigger Radiant Victory', FCVAR_CHEAT )

  self._currentRound = nil
  GameRules:GetGameModeEntity():SetModifyGoldFilter(Dynamic_Wrap(GameMode, "FilterGold"), self)
  GameRules:GetGameModeEntity():SetDamageFilter(Dynamic_Wrap(GameMode, "FilterDamage"), self)
  GameRules:GetGameModeEntity():SetExecuteOrderFilter( Dynamic_Wrap(GameMode, "FilterOrder"), self)

  CustomGameEventManager:RegisterListener("player_voted", Dynamic_Wrap(GameMode, "OnPlayerVoted"))

  DebugPrint('[BAREBONES] Done loading Barebones gamemode!\n\n')
end

function GameMode:_CheckForRoundEnd()
  if GameRules:State_Get() ~= DOTA_GAMERULES_STATE_GAME_IN_PROGRESS then
    return
  end

  if self.roundOver then
    return
  end

  local allGoodGuysDead = true
  local allBadGuysDead = true
  for nPlayerID = 0, DOTA_MAX_PLAYERS-1 do
    if PlayerResource:GetTeam(nPlayerID) == DOTA_TEAM_GOODGUYS then
      if not PlayerResource:HasSelectedHero(nPlayerID) then
        allGoodGuysDead = false
      else
        local hero = PlayerResource:GetSelectedHeroEntity(nPlayerID)
        if hero and hero:IsAlive() then
          allGoodGuysDead = false
        end
      end
    elseif PlayerResource:GetTeam(nPlayerID) == DOTA_TEAM_BADGUYS then
      if not PlayerResource:HasSelectedHero(nPlayerID) then
        allBadGuysDead = false
      else
        local hero = PlayerResource:GetSelectedHeroEntity(nPlayerID)
        if hero and hero:IsAlive() then
          allBadGuysDead = false
        end
      end
    end
  end

  if self.tomePlanted then
    if self._tomePlantingTeam == DOTA_TEAM_GOODGUYS then
      if allBadGuysDead then
        self:_GoodGuysWinRound()
      end
    elseif self._tomePlantingTeam == DOTA_TEAM_BADGUYS then
      if allGoodGuysDead then
        self:_BadGuysWinRound()
      end
    end
  else
    if allBadGuysDead then
      self:_GoodGuysWinRound()
    elseif allGoodGuysDead then
      self:_BadGuysWinRound()
    end
  end
end

function GameMode:_BadGuysWinRound()
  self.roundOver = true
  self:_DestroyPlantTimers()

  self._badGuyRoundCount = self._badGuyRoundCount + 1
  self._badGuyLosingStreak = 0
  self._goodGuyLosingStreak = self._goodGuyLosingStreak + 1

  CustomGameEventManager:Send_ServerToAllClients("dire_win_round", {score=self._badGuyRoundCount})
  Notifications:BottomToAll({text="Dire wins the round.", duration=3.0})

  Timers:CreateTimer(1.5, function()
    if self._tomePlantingTeam == DOTA_TEAM_GOODGUYS then
      EmitGlobalSound("Announcer.Radio.CTWin")
    else
      EmitGlobalSound("Announcer.Radio.TWin")
    end
  end)

  if self._badGuyRoundCount == self.rounds_to_win then
    self:_BadGuysWinGame()
  else
    self:_PrepareNextRound()
  end
end

function GameMode:_GoodGuysWinRound()
  self.roundOver = true
  self:_DestroyPlantTimers()

  self._goodGuyRoundCount = self._goodGuyRoundCount + 1
  self._goodGuyLosingStreak = 0
  self._badGuyLosingStreak = self._badGuyLosingStreak + 1

  CustomGameEventManager:Send_ServerToAllClients("radiant_win_round", {score=self._goodGuyRoundCount})
  Notifications:BottomToAll({text="Radiant wins the round.", duration=3.0})

  Timers:CreateTimer(1.5, function()
    if self._tomePlantingTeam == DOTA_TEAM_GOODGUYS then
      EmitGlobalSound("Announcer.Radio.TWin")
    else
      EmitGlobalSound("Announcer.Radio.CTWin")
    end
  end)

  if self._goodGuyRoundCount == self.rounds_to_win then
    self:_GoodGuysWinGame()
  else
    self:_PrepareNextRound()
  end
end

function GameMode:_BadGuysWinGame()
  CustomGameEventManager:Send_ServerToAllClients("game_end", {})
  GameRules:MakeTeamLose(DOTA_TEAM_GOODGUYS)
  for nPlayerID = 0, DOTA_MAX_TEAM_PLAYERS-1 do
    if PlayerResource:HasSelectedHero(nPlayerID) then
      if PlayerResource:GetTeam(nPlayerID) == DOTA_TEAM_GOODGUYS then
        EmitAnnouncerSoundForPlayer("announcer_ann_custom_end_04", nPlayerID)
      else
        EmitAnnouncerSoundForPlayer("announcer_ann_custom_end_02", nPlayerID)
      end
    end
  end
end

function GameMode:_GoodGuysWinGame()
  CustomGameEventManager:Send_ServerToAllClients("game_end", {})
  GameRules:MakeTeamLose(DOTA_TEAM_BADGUYS)
  for nPlayerID = 0, DOTA_MAX_TEAM_PLAYERS-1 do
    if PlayerResource:HasSelectedHero(nPlayerID) then
      if PlayerResource:GetTeam(nPlayerID) == DOTA_TEAM_GOODGUYS then
        EmitAnnouncerSoundForPlayer("announcer_ann_custom_end_02", nPlayerID)
      else
        EmitAnnouncerSoundForPlayer("announcer_ann_custom_end_04", nPlayerID)
      end
    end
  end
end

function GameMode:_RefreshPlayers()
  for nPlayerID = 0, DOTA_MAX_TEAM_PLAYERS-1 do
    if PlayerResource:HasSelectedHero(nPlayerID) then
      local hero = PlayerResource:GetSelectedHeroEntity(nPlayerID)
      if not hero:IsAlive() then
        hero:RespawnHero(false, false, false)
      end
      hero:Purge(true, true, false, true, false)
      for _, modifier in pairs(hero:FindAllModifiersByName("modifier_slark_essence_shift_debuff")) do
        modifier:Destroy()
      end
      if hero:FindModifierByName("modifier_slark_essence_shift_debuff_counter") ~= nil then
        hero:FindModifierByName("modifier_slark_essence_shift_debuff_counter"):Destroy()
      end
      if hero:FindModifierByName("modifier_slark_essence_shift") ~= nil then
        hero:FindModifierByName("modifier_slark_essence_shift"):SetStackCount(0)
      end
      hero:SetHealth(hero:GetMaxHealth())
      hero:SetMana(hero:GetMaxMana())
      hero:Stop()

      local nthIndex = self:_FindNthIndexOfPlayer(hero)
      if PlayerResource:GetTeam(nPlayerID) == DOTA_TEAM_GOODGUYS then
        if self._tomePlantingTeam == DOTA_TEAM_GOODGUYS then
          hero:SetOrigin(self:_SpawnLocation(DOTA_TEAM_GOODGUYS, nthIndex))
        else
          hero:SetOrigin(self:_SpawnLocation(DOTA_TEAM_BADGUYS, nthIndex))
        end
      else
        if self._tomePlantingTeam == DOTA_TEAM_GOODGUYS then
          hero:SetOrigin(self:_SpawnLocation(DOTA_TEAM_BADGUYS, nthIndex))
        else
          hero:SetOrigin(self:_SpawnLocation(DOTA_TEAM_GOODGUYS, nthIndex))
        end
      end
    end
  end
end

function GameMode:_PrepareNextRound()
  self._currentRound = self._currentRound + 1
  Timers:CreateTimer(3,
    function()
      self:_DetermineTomePlanter()
      self:_CleanInventorySpecialItems()
      self:_CleanDroppedSpecialItems()
      self:_ClearSpawnAreaMines()
      self:_DestroyPlantedTome()
      self:_RefreshPlayers()
      self:_MakeAllHeroesSleep()
      self:_RetargetPlayerCameras()
      self:_SetGoldBountyAndXPForRound(self._currentRound)
      self:_AwardRoundGold()
      self:_SetTimeOfDay(self._currentRound)
      self:_GrantSpecialItems()
      self.tomePlanted = false
      EmitAnnouncerSound("announcer_ann_custom_round_begin_02")
      CustomGameEventManager:Send_ServerToAllClients("pre_round_start", {})
    end)

  Timers:CreateTimer(23,
    function()
      EmitAnnouncerSound("announcer_ann_custom_timer_sec_05")
    end)

  --Timers:CreateTimer(26.5,
  --  function()
  --    self:_AnnounceRoundSound(self._currentRound)
  --  end)

  Timers:CreateTimer(28,
    function()
      GameMode:_WakeUpHeroes()
      self.roundOver = false

      self:_StartPlantTimers()
      CustomGameEventManager:Send_ServerToAllClients("round_start", {})

      EmitAnnouncerSound("announcer_ann_custom_begin")
      Notifications:BottomToAll({text="Round " .. self._currentRound .. " begin!", duration = 3.0})
    end)

    Timers:CreateTimer(30, function()
      EmitAnnouncerSound("announcer_ann_custom_timer_02")
    end)
end

function GameMode:_ClearSpawnAreaMines()
  local techies = Entities:FindAllByName("npc_dota_hero_techies")[1]
  local mine_cleared = false
  local mines = Entities:FindAllByClassname("npc_dota_techies_mines")
  for _, spawn_area_trigger in pairs(Entities:FindAllByName("trigger_spawn_area_marker")) do
    local mines = Entities:FindAllByClassnameWithin("npc_dota_techies_mines", spawn_area_trigger:GetAbsOrigin(), 512)
    for _, mine in pairs(mines) do
      mine_cleared = true
      mine:RemoveSelf()
    end
  end
  if techies ~= nil and mine_cleared then
    Timers:CreateTimer(3, function()
      Notifications:Bottom(techies:GetPlayerOwner(), {text="Mines cleared from spawn area. Nice try, smart ass.", duration = 4.0})
    end)
  end
end

function GameMode:_HintTomePlantLocationsToPlayer(nPlayerID)
  local hero = PlayerResource:GetSelectedHeroEntity(nPlayerID)
  local time = -1
  for _, target in pairs(Entities:FindAllByName("hint_target")) do
    time = time + 2
    local pos = target:GetAbsOrigin()
    Timers:CreateTimer(time, function()
      MinimapEvent(hero:GetTeam(), hero, pos.x, pos.y, DOTA_MINIMAP_EVENT_HINT_LOCATION, 2)
    end)
  end
end

function GameMode:_HintTomePlantLocations()
  for nPlayerID = 0, DOTA_MAX_TEAM_PLAYERS-1 do
    if PlayerResource:HasSelectedHero(nPlayerID) then
      self:_HintTomePlantLocationsToPlayer(nPlayerID)
    end
  end
end

function GameMode:_StartPlantTimers()
  self.thirty_second_warning_timer = Timers:CreateTimer(90, function()
    EmitAnnouncerSound("announcer_ann_custom_timer_sec_30")
  end)

  self.ten_second_warning_timer = Timers:CreateTimer(110, function()
    EmitAnnouncerSound("announcer_ann_custom_timer_sec_10")
  end)

  self.time_expired_timer = Timers:CreateTimer(120, function()
    if not self.roundOver then
      EmitAnnouncerSound("announcer_ann_custom_time_expired")

      if self._tomePlantingTeam == DOTA_TEAM_BADGUYS then
        self:_GoodGuysWinRound()
      else
        self:_BadGuysWinRound()
      end
    end
  end)
end

function GameMode:_DestroyPlantTimers()
  if Timers.timers[self.thirty_second_warning_timer] ~= nil then
    Timers:RemoveTimer(self.thirty_second_warning_timer)
  end

  if Timers.timers[self.ten_second_warning_timer] ~= nil then
    Timers:RemoveTimer(self.ten_second_warning_timer)
  end

  if Timers.timers[self.time_expired_timer] ~= nil then
    Timers:RemoveTimer(self.time_expired_timer)
  end
end

function GameMode:_DetermineTomePlanter()
  if self._tomePlantingTeam == DOTA_TEAM_BADGUYS then
    self._tomePlantingTeam = DOTA_TEAM_GOODGUYS
    local player_count = PlayerResource:GetPlayerCountForTeam(DOTA_TEAM_GOODGUYS)
    self._tomePlantingGoodGuysIndex = (self._tomePlantingGoodGuysIndex + 1) % player_count
  else
    self._tomePlantingTeam = DOTA_TEAM_BADGUYS
    local player_count = PlayerResource:GetPlayerCountForTeam(DOTA_TEAM_BADGUYS)
    self._tomePlantingBadGuysIndex = (self._tomePlantingBadGuysIndex + 1) % player_count
  end
end

function GameMode:_GrantSpecialItems()
  local index
  local opposing_team
  if self._tomePlantingTeam == DOTA_TEAM_GOODGUYS then
    opposing_team = DOTA_TEAM_BADGUYS
    index = self._tomePlantingGoodGuysIndex
  else
    opposing_team = DOTA_TEAM_GOODGUYS
    index = self._tomePlantingBadGuysIndex
  end

  local playerID = PlayerResource:GetNthPlayerIDOnTeam(self._tomePlantingTeam, index+1)
  if PlayerResource:HasSelectedHero(playerID) then
    local hero = PlayerResource:GetSelectedHeroEntity(playerID)
    if hero:GetNumItemsInInventory() ~= 6 then
      hero:AddItemByName("item_explosive_tome")
    else
      local newItem = CreateItem("item_explosive_tome", nil, nil)
      CreateItemOnPositionSync(hero:GetOrigin(), newItem)
    end
  end

  playerID = PlayerResource:GetNthPlayerIDOnTeam(opposing_team, index+1)
  if PlayerResource:HasSelectedHero(playerID) then
    local hero = PlayerResource:GetSelectedHeroEntity(playerID)
    if hero:GetNumItemsInInventory() ~= 6 then
      hero:AddItemByName("item_defusal_wand")
    else
      local newItem = CreateItem("item_defusal_wand", nil, nil)
      CreateItemOnPositionSync(hero:GetOrigin(), newItem)
    end
  end

  Timers:CreateTimer(2, function()
    Notifications:BottomToTeam(self._tomePlantingTeam, {text="Your team is Ts.", duration = 3.0})
    Notifications:BottomToTeam(opposing_team, {text="Your team is CTs.", duration = 3.0})
  end)
end

function GameMode:_RoundNumToTomeAbilityLevel()
  return self.tome_level_table[self._currentRound]
end

function GameMode:_TomePlanted(tome)
  self.tomePlanted = true
  self.tome = tome
  CustomGameEventManager:Send_ServerToAllClients("tome_planted", {})

  EmitGlobalSound("Announcer.Radio.BombPlanted")
  --EmitAnnouncerSound("announcer_ann_custom_generic_alert_63")
  self:_DestroyPlantTimers()
end

function GameMode:_TomeExploded(tome_team)
  self.tome = nil
  if not self.roundOver then
    if tome_team == DOTA_TEAM_GOODGUYS then
      self:_GoodGuysWinRound()
    else
      self:_BadGuysWinRound()
    end
  end
end

function GameMode:_TomeDefused(tome_team)
  self.tome = nil
  if not self.roundOver then
    EmitGlobalSound("Announcer.Radio.BombDefused")
    if tome_team == DOTA_TEAM_GOODGUYS then
      self:_BadGuysWinRound()
    else
      self:_GoodGuysWinRound()
    end
  end
end

function GameMode:_CleanInventorySpecialItems()
  local items_to_remove = {}

  for nPlayerID = 0, DOTA_MAX_TEAM_PLAYERS-1 do
    if PlayerResource:HasSelectedHero(nPlayerID) then
      local hero = PlayerResource:GetSelectedHeroEntity(nPlayerID)
      for i = 0, 5 do
        local item = hero:GetItemInSlot(i)
        if item ~= nil then
          if item:GetAbilityName() == 'item_explosive_tome' then
            hero:RemoveItem(item)
          elseif item:GetAbilityName() == 'item_defusal_wand' then
            hero:RemoveItem(item)
          end
        end
      end
    end
  end
end

function GameMode:_CleanDroppedSpecialItems()
  local dropped_items_count = GameRules:NumDroppedItems()
  local physical_items_to_remove = {}
  local items_to_remove = {}
  for i = 0, dropped_items_count-1 do
    local physical_item = GameRules:GetDroppedItem(i)
    if physical_item ~= nil then
      local item = physical_item:GetContainedItem()
      if (item:GetAbilityName() == 'item_explosive_tome') then
        table.insert(items_to_remove, item)
        table.insert(physical_items_to_remove, physical_item)
      elseif (item:GetAbilityName() == 'item_defusal_wand') then
        table.insert(items_to_remove, item)
        table.insert(physical_items_to_remove, physical_item)
      end
    end
  end

  for _, item in pairs(items_to_remove) do
    item:RemoveSelf()
  end

  for _, physical_item in pairs(physical_items_to_remove) do
    physical_item:RemoveSelf()
  end
end

function GameMode:_DestroyPlantedTome()
  if self.tome ~= nil then
    self.tome:ForceKill(false)
    self.tome = nil
  end
end

function GameMode:_AwardRoundGold()
  for nPlayerID = 0, DOTA_MAX_TEAM_PLAYERS-1 do
    if PlayerResource:HasSelectedHero(nPlayerID) then
      local hero = PlayerResource:GetSelectedHeroEntity(nPlayerID)
      local goldAward = 0
      local awardReason = ""
      if PlayerResource:GetTeam(nPlayerID) == DOTA_TEAM_GOODGUYS then
        if self._goodGuyLosingStreak == 0 then
          goldAward = WIN_ROUND_GOLD
          awardReason = " for winning."
        else
          goldAward = LOSING_STREAK_GOLD[self._goodGuyLosingStreak]
          if goldAward == nil then goldAward = 2000 end
          awardReason = " for losing streak of " .. self._goodGuyLosingStreak .. "."
        end
      elseif PlayerResource:GetTeam(nPlayerID) == DOTA_TEAM_BADGUYS then
        if self._badGuyLosingStreak == 0 then
          goldAward = WIN_ROUND_GOLD
          awardReason = " for winning."
        else
          goldAward = LOSING_STREAK_GOLD[self._badGuyLosingStreak]
          if goldAward == nil then goldAward = 2000 end
          awardReason = " for losing streak of " .. self._badGuyLosingStreak .. "."
        end
      end
      hero:ModifyGold(goldAward, true, 0)
      Notifications:Bottom(PlayerResource:GetPlayer(nPlayerID), {text="Awarded " .. goldAward .. " gold " .. awardReason, duration = 3.0})
    end
  end
end

function GameMode:_MakeHeroSleep(hero)
  self._sleeper:SetCursorCastTarget(hero)
  self._sleepAbility:OnSpellStart()
end

function GameMode:_MakeAllHeroesSleep()
  self.sleep_timer = Timers:CreateTimer(function()
    for nPlayerID = 0, DOTA_MAX_TEAM_PLAYERS-1 do
      if PlayerResource:HasSelectedHero(nPlayerID) then
        local hero = PlayerResource:GetSelectedHeroEntity(nPlayerID)
        self:_MakeHeroSleep(hero)
      end
    end
    return 1.0
  end)
end

function GameMode:_SetTimeOfDay(roundNum)
  local round_num_mod_four = (roundNum-1) % 4
  if (round_num_mod_four <= 1) then
    GameRules:SetTimeOfDay(0.25)
    if (round_num_mod_four == 0) then
      Timers:CreateTimer(5, function()
        EmitAnnouncerSound("announcer_ann_custom_time_alert_05")
      end)
    end
  else
    GameRules:SetTimeOfDay(0.75)
    if (round_num_mod_four == 2) then
      Timers:CreateTimer(5, function()
        EmitAnnouncerSound("announcer_ann_custom_time_alert_01")
      end)
    end
  end
end

function GameMode:_SpawnLocation(team, index)
  local entity_name = ""
  if team == DOTA_TEAM_GOODGUYS then
    entity_name = "trigger_radiant_spawn_" .. index
  elseif team == DOTA_TEAM_BADGUYS then
    entity_name = "trigger_dire_spawn_" .. index
  end
  return GetGroundPosition(Entities:FindAllByName(entity_name)[1]:GetOrigin(), nil)
end

function GameMode:_WakeUpHeroes()
  Timers:RemoveTimer(self.sleep_timer)
  for nPlayerID = 0, DOTA_MAX_TEAM_PLAYERS-1 do
    if PlayerResource:HasSelectedHero(nPlayerID) then
      local hero = PlayerResource:GetSelectedHeroEntity(nPlayerID)
      hero:RemoveModifierByName("dummy_sleep_modifier")
    end
  end
end

function GameMode:_SetGoldBountyAndXPForRound(roundNum)
  for nPlayerID = 0, DOTA_MAX_TEAM_PLAYERS-1 do
    if PlayerResource:HasSelectedHero(nPlayerID) then
      local hero = PlayerResource:GetSelectedHeroEntity(nPlayerID)
      local team = PlayerResource:GetTeam(nPlayerID)
      if team == DOTA_TEAM_GOODGUYS and self._goodGuyLosingStreak == 0 then
        hero:SetMinimumGoldBounty(self.gold_bounty_table[roundNum] * 1.5)
        hero:SetMaximumGoldBounty(self.gold_bounty_table[roundNum] * 1.5)
      elseif team == DOTA_TEAM_BADGUYS and self._badGuyLosingStreak == 0 then
        hero:SetMinimumGoldBounty(self.gold_bounty_table[roundNum] * 1.5)
        hero:SetMaximumGoldBounty(self.gold_bounty_table[roundNum] * 1.5)
      else
        hero:SetMinimumGoldBounty(self.gold_bounty_table[roundNum])
        hero:SetMaximumGoldBounty(self.gold_bounty_table[roundNum])
      end

      local level = hero:GetLevel()
      while level < self.xp_table[roundNum] do
        hero:AddExperience(100, 0, false, true)
        level = hero:GetLevel()
      end
    end
  end
end

function GameMode:_RetargetPlayerCameras()
  for nPlayerID = 0, DOTA_MAX_TEAM_PLAYERS-1 do
    if PlayerResource:HasSelectedHero(nPlayerID) then
      local hero = PlayerResource:GetSelectedHeroEntity(nPlayerID)
      PlayerResource:SetCameraTarget(nPlayerID, hero)

      Timers:CreateTimer(0.1, function()
        PlayerResource:SetCameraTarget(nPlayerID, nil)
      end)
    end
  end
end

function GameMode:_RetargetPlayerCamera(nPlayerID)
  local hero = PlayerResource:GetSelectedHeroEntity(nPlayerID)
  PlayerResource:SetCameraTarget(nPlayerID, hero)

  Timers:CreateTimer(0.1, function()
    PlayerResource:SetCameraTarget(nPlayerID, nil)
  end)
end

ANNOUNCE_ROUND_SOUNDS = {"announcer_ann_custom_round_01",
                         "announcer_ann_custom_round_02",
                         "announcer_ann_custom_round_03",
                         "announcer_ann_custom_round_04",
                         "announcer_ann_custom_round_05",
                         "announcer_ann_custom_round_06",
                         "announcer_ann_custom_round_07",
                         "announcer_ann_custom_round_08",
                         "announcer_ann_custom_round_09",
                         "announcer_ann_custom_round_10",
                         "announcer_ann_custom_round_final"}

function GameMode:_AnnounceRoundSound(roundNum)
  EmitAnnouncerSound(ANNOUNCE_ROUND_SOUNDS[roundNum])
end

function GameMode:_FindNthIndexOfPlayer(hero)
  local playerID = hero:GetPlayerID()
  local team = hero:GetTeam()

  for i = 0, 5 do
    local nthPlayerID = PlayerResource:GetNthPlayerIDOnTeam(team, i)
    if nthPlayerID == playerID then
      return i
    end
  end

  return -1
end

function GameMode:FilterGold( filterTable )
  if filterTable.reason_const == DOTA_ModifyGold_HeroKill then
    return (filterTable.gold == (self.gold_bounty_table[self._currentRound]) or filterTable.gold == (self.gold_bounty_table[self._currentRound] * 1.5))
  end
  return true
end

function GameMode:FilterDamage( filterTable )
  if filterTable.entindex_inflictor_const ~= nil then
    local hero = EntIndexToHScript(filterTable.entindex_attacker_const)
    if hero:HasItemInInventory("item_wand_of_merlin") then
      filterTable.damage = filterTable.damage * 1.5
    end
  end

  if filterTable.entindex_victim_const ~= nil then
    local victim = EntIndexToHScript(filterTable.entindex_victim_const)
    if victim:GetUnitName() == "npc_explosive_tome" then
      filterTable.damage = 1
    end
  end
  return true
end

function GameMode:FilterOrder(filterTable)
  local order_type = filterTable.order_type
  local player = PlayerResource:GetPlayer(filterTable.issuer_player_id_const)
  local unit = EntIndexToHScript(filterTable.units['0'])
  local ability = EntIndexToHScript(filterTable.entindex_ability)

  if order_type == DOTA_UNIT_ORDER_PICKUP_ITEM then
    local team = unit:GetTeam()
    local target_physical_item = EntIndexToHScript(filterTable.entindex_target)
    if target_physical_item:GetContainedItem():GetAbilityName() == 'item_explosive_tome' then
      if team ~= self._tomePlantingTeam then
        EmitSoundOnClient("General.CastFail_InvalidTarget_Other", player)
        return
      end
    elseif target_physical_item:GetContainedItem():GetAbilityName() == 'item_defusal_wand' then
      if team == self._tomePlantingTeam then
        EmitSoundOnClient("General.CastFail_InvalidTarget_Other", player)
        return
      end
    end
  elseif order_type == DOTA_UNIT_ORDER_GLYPH then
    return
  elseif order_type == DOTA_UNIT_ORDER_CAST_POSITION then
    --prevent structure-like abilities from being placed in hero clip areas
    local ability_name = ability:GetName()
    if ability_name == "item_ward_sentry" or ability_name == "undying_tombstone" or ability_name == "shadow_shaman_mass_serpent_ward" or ability_name == "venomancer_plague_ward" then
      for _, thing in pairs(Entities:FindAllInSphere(Vector(filterTable.position_x, filterTable.position_y, filterTable.position_z), 96)) do
        if thing:GetName() == "trigger_hero_clip" then
          EmitSoundOnClient("General.CastFail_InvalidTarget_Other", player)
          Notifications:Bottom(player, {text="Invalid cast area.", duration=2.0} )
          return
        end
      end
    end
  elseif order_type == DOTA_UNIT_ORDER_CAST_TARGET then
    -- prevent defusal wand from targeting anything except the explosive ward
    if ability:GetName() == "item_defusal_wand" then
      local target_unit = EntIndexToHScript(filterTable.entindex_target)
      if target_unit:GetUnitName() ~= "npc_explosive_tome" then
        Notifications:Bottom(player, {text="Invalid target.", duration=2.0})
        return
      end
    end
  end

  return true
end

-- This is an example console command
function GameMode:ExampleConsoleCommand()
  print( '******* Example Console Command ***************' )
  local cmdPlayer = Convars:GetCommandClient()
  if cmdPlayer then
    local playerID = cmdPlayer:GetPlayerID()
    if playerID ~= nil and playerID ~= -1 then
      -- Do something here for the player who called this command
      PlayerResource:ReplaceHeroWith(playerID, "npc_dota_hero_viper", 1000, 1000)
    end
  end

  print( '*********************************************' )
end

function GameMode:TriggerGoodGuysWin()
  GameRules.GameMode:_GoodGuysWinGame()
end
