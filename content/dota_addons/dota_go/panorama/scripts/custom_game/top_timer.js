var startTime = -1;
var duration = 120;
var deactivated = false;
var TIMER_INTERVAL = 0.05;

function StartRoundTimer(msg)
{
  duration = 120;
  deactivated = false;
  startTime = Game.GetGameTime();
  UpdateTimer();
}

function StartPreRoundTimer(msg)
{
  duration = 25;
  deactivated = false;
  startTime = Game.GetGameTime();
  UpdateTimer();
}

function UpdateTimer()
{
  if (!deactivated)
  {
    var time = Game.GetGameTime() - startTime;
    var remaining = Math.ceil(duration - time);
    SetTimeRemaining(FormatTime(remaining));
    if (time < duration)
      $.Schedule(TIMER_INTERVAL, function(){UpdateTimer();});
  }
}

function FormatTime(seconds)
{
  var remainder = seconds % 3600;
	var minutes = Math.floor(remainder / 60);
	var seconds = Math.floor(remainder % 60);

  var s = "";
	var m = "";
  if (seconds < 10)
    s = "0";
  if (minutes < 10)
    m = "0";

    return m + minutes + ":" + s + seconds;
}

function DeactivateTimer(msg)
{
  deactivated = true;
  SetTimeRemaining("--:--")
}

function SetTimeRemaining(time)
{
  $('#Timer').text = time;
}

(function () {
  GameEvents.Subscribe( "round_start", StartRoundTimer );
  GameEvents.Subscribe( "pre_round_start", StartPreRoundTimer );
  GameEvents.Subscribe( "tome_planted", DeactivateTimer );
})();
