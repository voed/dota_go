var votePanelContainer;
var countdownContainer;
var countdown;

function Setup()
{
  votePanelContainer = $("#VotePanelContainer");
  countdownContainer = $("#CountdownContainer");
  countdown = $("#Countdown");
  votePanelContainer.visible = false;
  countdownContainer.visible = false;
}

function DisplayVotePanel()
{
  votePanelContainer.visible = true;
  countdownContainer.visible = true;
}

function HideVotePanel()
{
  votePanelContainer.visible = false;
  countdownContainer.visible = false;
}

function PlayerVoted(category)
{
  var data = {};

  data['category'] = category;

  $.Msg(data);

  GameEvents.SendCustomGameEventToServer("player_voted", { "data": data });
}

function DisplayVote(data)
{
  $.Msg(data);
  var playerID = parseInt(data.playerID);
  var voteCategory = data.category;
  var playerData = Game.GetPlayerInfo(playerID);
  $.Msg(playerData);
  var panel = $("#" + voteCategory + "VoteDisplay");

  var avatar = $.CreatePanel('DOTAAvatarImage', panel, '');
  avatar.steamid = playerData.player_steamid;

  if (Game.GetLocalPlayerID() == playerID) {

      var buttonPanel = $("#" + voteCategory + "Length");
      buttonPanel.AddClass('ButtonPressed');
  }
}

function UpdateCountdown(data)
{
  var value = data.value;
  countdown.text = value;
}

function ButtonHover(name)
{
  var panel = $("#" + name + "Length");

  panel.AddClass('ButtonHover');
}

function StopButtonHover(name)
{
  var panel = $("#" + name + "Length");

  panel.RemoveClass('ButtonHover');
}

(function() {
  Setup();
  GameEvents.Subscribe("vote_panel_display", DisplayVotePanel);
  GameEvents.Subscribe("vote_panel_hide", HideVotePanel);
  GameEvents.Subscribe("display_vote", DisplayVote);
  GameEvents.Subscribe("update_countdown", UpdateCountdown);
})();
